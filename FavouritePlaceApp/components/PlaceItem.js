import { View, Text, Pressable , Image} from 'react-native'
import React from 'react'

export default function PlaceItem({place, onSelect}) {
  return (
    <Pressable onPress={onSelect}>
        <Image source={{uri: place.iamgeuri}}/>
        <View>
            <Text>{place.title}</Text>
            <Text>{place.address}</Text>
        </View>
    </Pressable>
  )
}
