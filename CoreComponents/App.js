import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, ScrollView, Image, TextInput, Button } from 'react-native';
import ViewComponent from './components/viewComponent';
import TextComponent from './components/textComponent';
import ImageComponent from './components/imageComponent';
import TextInputComponent from './components/textInputComponent';

export default function App() {
  return (
    <View>
      {/* <ViewComponent></ViewComponent> */}
      {/* <ImageComponent></ImageComponent> */}
      <TextInputComponent></TextInputComponent>
      
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
