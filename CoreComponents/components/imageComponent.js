import React from 'react'
import {View, Image, StyleSheet } from 'react-native';

const ImageComponent = () => {
    return (
        <View>
            <Image
            style = {StyleSheet.logoContain}
            source = {require('../assets/favicon.png')}/>
            <Image
            style = {styles.logoStretch}
            source= {{uri: 'https://about.gitlab.com/images/topics/devops-lifecycle.png'}}/>
        </View>
    )
}

const styles = StyleSheet.create({
    logoContain: {
        width: 200,
        height: 100,
        resizeMode: 'contain',
    },
    logoStretch: {
        width: 200,
        height: 100,
        resizeMode: 'stretch',
    },
});

export default ImageComponent