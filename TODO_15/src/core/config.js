// Import the functions you need from the SDKs you need
import firebase from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
export const firebaseConfig = {
  apiKey: "AIzaSyCwtZQ2WujUW1-NfJuLwiHPPXrSeJ0-ToM",
  authDomain: "myapp-9f674.firebaseapp.com",
  projectId: "myapp-9f674",
  storageBucket: "myapp-9f674.appspot.com",
  messagingSenderId: "889510631951",
  appId: "1:889510631951:web:706838fa3da77a0996e17a"
};

// Initialize Firebase
// const app = initializeApp(firebaseConfig);
export default firebase;