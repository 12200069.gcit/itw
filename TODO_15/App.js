import { Image, StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-native-paper';
import { theme } from './src/core/theme';
import Button from './src/components/Button';
import TextInput from './src/components/TextInput';
import DrawerContent from './src/components/DrawerContent';
import Header from './src/components/Header';
import { NavigationContainer, TabActions } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';
import firebase from 'firebase/app';
import { 
  StartScreen, 
  LoginScreen, 
  RegisterScreen, 
  ResetPasswordScreen,
  ProfileScreen,
  HomeScreen,
 } from './src/Screens';
import { firebaseConfig } from './src/core/config';

if (!firebase.apps.length){
  firebase.initializeApp(firebaseConfig);
}
const Stacks = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <Provider theme= {theme}>
      <NavigationContainer>
        <Stacks.Navigator
          initialRouteName = 'StartScreen'
          screenOptions = {{headerShown: false}}
      >
          <Stacks.Screen name='StartScreen' component={StartScreen} />
          <Stacks.Screen name='LoginScreen' component={LoginScreen} />
          <Stacks.Screen name='RegisterScreen' component={RegisterScreen} />
          <Stacks.Screen name='ResetPasswordScreen' component={ResetPasswordScreen} />
          <Stacks.Screen name='HomeScreen' component={DrawerNavigator} />
      </Stacks.Navigator>
      </NavigationContainer>
    </Provider>
  );
}

function BottomNavigation() {
  return (
    <Tab.Navigator>
      <Tab.Screen 
        name='Home' 
        component={HomeScreen}
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image 
                style={{ width: size, height: size }}
                source={
                  require('./assets/Home-icon.png')
                }
                />
            );
          },
        }} />
      <Tab.Screen 
        name='Profile' 
        component={ProfileScreen}
        options={{
          tabBarIcon: ({size}) => {
            return (
              <Image
                style={{ width: size, height: size }}
                source= {
                  require('./assets/setting-icon.png')
                }
                />
            );
          },
        }} />
    </Tab.Navigator>
  )
}

const DrawerNavigator = () => {
  return(
    <Drawer.Navigator drawerContent={DrawerContent}>
      <Drawer.Screen name='HomeScreen' component={BottomNavigation} />
    </Drawer.Navigator>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

