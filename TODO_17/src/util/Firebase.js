import { firebase } from '@firebase/app'

const firebaseConfig = {
  apiKey: "AIzaSyDjqfpLBXNEqxiUfSuDMXAFNorwuqBpTSY",
  authDomain: "fir-5fb5f.firebaseapp.com",
  projectId: "fir-5fb5f",
  storageBucket: "fir-5fb5f.appspot.com",
  messagingSenderId: "985545611932",
  appId: "1:985545611932:web:60b5a529088a2a67276b59",
  measurementId: "G-2FMNW0XSQ4"
};

firebase.initializeApp(firebaseConfig)

export default firebase;
