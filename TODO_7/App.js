import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';

export default function App() {
  return (
    <View style={styles.container}>
      <View style = {styles.box01}></View>
      <View style = {styles.box02}></View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  box01:{
    height: 150,
    width: 150,
    backgroundColor: 'red',
  },
  box02:{
    height: 150,
    width: 150,
    backgroundColor: 'royalblue',
  }
});
