import React, {useState} from "react";
import {Text, View, TextInput, StyleSheet} from 'react-native';

var Input = () => {
    const [text, textName] = useState('');
    return (
        <View>
            <Text>What is your name?</Text>
            <Text> {text.split(' ').join(' ')}</Text>

            <TextInput
            style = {{borderColor: 'black', borderWidth: 2, width: 300}}
            secureTextEntry = {true}
            onChangeText = {(val)=> textName(val)}/>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
export default Input;