import React from "react";
import {View, Text, StyleSheet} from 'react-native';

var Todo = ()=> {
    return (
        <View style= {StyleSheet.container}>
            <Text style = {StyleSheet.b}>The <Text style={{fontWeight: 'bold'}}>quick brown fox 
            </Text> jumps over the lazy dog</Text>
        </View>
    )
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'flex-start',
        justifyContent: 'center',
        backgroundColor: '#fff'
    },
    b: {
        paddingRight: 80,
        fontSize: 50,
    }
});
export default Todo;