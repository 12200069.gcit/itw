import React from 'react';
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native';

export const AspirationItem = (props) => {
    return (
        <TouchableOpacity OnPress = {props.onDelete.bind(this, props.id)}>
            <View style = {styles.listItems}>
                <Text>{props.title}</Text>
            </View>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    listItem: {
        padding: 10,
        marginVertical: 10,
        backgroundColor: '#ccc',
        borderColor: 'black',
        borderWidth: 1,
    }
})