import React, {useState} from 'react';
import {StyleSheet, Text, View, TextInput, Button, Modal, TouchableOpacity} from 'react-native';

const AspirationInput = (props) => {
    const [enteredAspiration, setEnteredAspiration] = useState('');

    const AspirationInputHandler = (enteredText) => {
        setEnteredAspiration(enteredText);
    };
    return (
        <Modal visible = {props.visible} animationType = "slide">
            <View style = {styles.inputContainer}>
                <TextInput
                    placeholder= "My Aspiration from this module"
                    style = {styles.input}
                    onChangeText = {AspirationInputHandler}
                    value = {enteredAspiration}></TextInput>

                    <View style = {{flexDirection: 'row', }}>
                        <TouchableOpacity onPress = {() => setEnteredAspiration('')}>
                            <Text style = {{color: '#E74C3C', fontWeight: 'bold'}}>CANCEL </Text>
                        </TouchableOpacity>

                        <TouchableOpacity onPress = {() => props.onAddAspiration(enteredAspiration)}>
                            <Text style = {{color: '#5DADE2', fontWeight: 'bold'}}> ADD</Text>
                        </TouchableOpacity>
                    </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    inputContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        marginBottom: 10,
    }
});

export default AspirationInput;